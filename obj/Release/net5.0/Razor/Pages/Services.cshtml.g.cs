#pragma checksum "D:\workspace\BInfotech\Pages\Services.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "91fa4f96dc4c1a3696461741166c001232ce1172"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(BInfoTech.Pages.Pages_Services), @"mvc.1.0.razor-page", @"/Pages/Services.cshtml")]
namespace BInfoTech.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\workspace\BInfotech\Pages\_ViewImports.cshtml"
using BInfoTech;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"91fa4f96dc4c1a3696461741166c001232ce1172", @"/Pages/Services.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24d7831bfe8b0e3c1c821b9414f5bc9ee09625c9", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Services : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\workspace\BInfotech\Pages\Services.cshtml"
  
    ViewData["Title"] = "Services";


#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<section id=""services"" class=""services section-bg"">
      <div class=""container"">

        <div class=""section-title"">
          <h2>Services</h2>
          <p>Necessitatibus eius consequatur ex aliquid fuga eum quidem</p>
        </div>

        <div class=""row"">
          <div class=""col-md-6"">
            <div class=""icon-box"">
              <i class=""bi bi-briefcase""></i>
              <h4><a href=""#"">Lorem Ipsum</a></h4>
              <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div>
          </div>
          <div class=""col-md-6 mt-4 mt-lg-0"">
            <div class=""icon-box"">
              <i class=""bi bi-card-checklist""></i>
              <h4><a href=""#"">Dolor Sitema</a></h4>
              <p>Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
            </div>
          </div>
          <div class=""col-md-6 mt-4"">
   ");
            WriteLiteral(@"         <div class=""icon-box"">
              <i class=""bi bi-bar-chart""></i>
              <h4><a href=""#"">Sed ut perspiciatis</a></h4>
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
          <div class=""col-md-6 mt-4"">
            <div class=""icon-box"">
              <i class=""bi bi-binoculars""></i>
              <h4><a href=""#"">Nemo Enim</a></h4>
              <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
          </div>
          <div class=""col-md-6 mt-4"">
            <div class=""icon-box"">
              <i class=""bi bi-brightness-high""></i>
              <h4><a href=""#"">Magni Dolore</a></h4>
              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>
          </div>
          <div class=""col-");
            WriteLiteral(@"md-6 mt-4"">
            <div class=""icon-box"">
              <i class=""bi bi-calendar4-week""></i>
              <h4><a href=""#"">Eiusmod Tempor</a></h4>
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
            </div>
          </div>
        </div>

      </div>
    </section>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<BInfoTech.Pages.ServicesModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<BInfoTech.Pages.ServicesModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<BInfoTech.Pages.ServicesModel>)PageContext?.ViewData;
        public BInfoTech.Pages.ServicesModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
