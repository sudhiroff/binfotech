#pragma checksum "D:\workspace\BInfotech\Pages\Component\Footer.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "46b7148c70452a515f23f66975d17e7f59720bb7"
// <auto-generated/>
#pragma warning disable 1591
namespace BInfoTech.Pages.Component
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
    public partial class Footer : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<footer id=\"footer\"><div class=\"footer-top\"><div class=\"container\"><div class=\"row\"><div class=\"col-lg-3 col-md-6 footer-contact\"><h3>Buildrock Infotech</h3>\r\n            <p>\r\n              A108 Adam Street <br>\r\n              New York, NY 535022<br>\r\n              United States <br><br>\r\n              <strong>Phone:</strong> +1 5589 55488 55<br>\r\n              <strong>Email:</strong> info@example.com<br></p></div>\r\n\r\n          <div class=\"col-lg-2 col-md-6 footer-links\"><h4>Useful Links</h4>\r\n            <ul><li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Home</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"/About\">About us</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"/Services\">Services</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"/Portfolio\">Portfolio</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"/Contact\">Contact</a></li></ul></div>\r\n\r\n          <div class=\"col-lg-3 col-md-6 footer-links\"><h4>Our Services</h4>\r\n            <ul><li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Web Design</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Web Development</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Product Management</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Marketing</a></li>\r\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Graphic Design</a></li></ul></div>\r\n\r\n          <div class=\"col-lg-4 col-md-6 footer-newsletter\"><h4>Join Our Newsletter</h4>\r\n            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>\r\n            <form action method=\"post\"><input type=\"email\" name=\"email\"><input type=\"submit\" value=\"Subscribe\"></form></div></div></div></div>\r\n\r\n    <div class=\"container d-md-flex py-4\"><div class=\"me-md-auto text-center text-md-start\"><div class=\"copyright\">\r\n          &copy; Copyright <strong><span>Buildrock Infotech</span></strong>. All Rights Reserved\r\n        </div>\r\n        <div class=\"credits\">\r\n          Designed by <a href=\"https://buildrock-infotech.com/\" target=\"_blank\">Buildrock Infotech</a></div></div>\r\n      <div class=\"social-links text-center text-md-right pt-3 pt-md-0\"><a href=\"#\" class=\"twitter\"><i class=\"bx bxl-twitter\"></i></a>\r\n        <a href=\"#\" class=\"facebook\"><i class=\"bx bxl-facebook\"></i></a>\r\n        <a href=\"#\" class=\"instagram\"><i class=\"bx bxl-instagram\"></i></a>\r\n        <a href=\"#\" class=\"google-plus\"><i class=\"bx bxl-skype\"></i></a>\r\n        <a href=\"#\" class=\"linkedin\"><i class=\"bx bxl-linkedin\"></i></a></div></div></footer>");
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
